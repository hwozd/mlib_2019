from dataset import DataSet
import numpy as np
from svm import train_svm
from nb import train_nb
from knn import train_knn
from EnsambleClassifier import train_ensamble

np.random.seed(42)
dataset = DataSet(path='../CTG.csv', scaled=True, pca_components=21)

score_nb, score_test_nb = train_nb(dataset)
score_knn, score_test_knn = train_knn(dataset, k=2)
score_svm, score_test_svm = train_svm(dataset, decision_function_shape='ovo')
score_test_ensamble = train_ensamble(dataset)
score_svm_best, score_test_svm_best = train_svm(dataset, decision_function_shape='ovo', kernel='linear', gamma=0.0037, C=2, max_iter=100000)

# << parameters from this:
#from svm import search_svm_kernel
#print('ovo:')
#search_svm_kernel(dataset, max_iter=10000, decision_function_shape='ovo')
#print('ovr')
#search_svm_kernel(dataset, max_iter=10000, decision_function_shape='ovr')
# -> 'linear', 'ovo'
#from svm import grid_search_svm
#grid_search_svm(dataset, decision_function_shape='ovo')
# -> gamma and c parameters

print('nb                  :')
print('  cross_val         : {} : std={} : {}'.format(score_nb.mean(), score_knn.std(), score_nb))
print('  test_data         : {}'.format(score_test_nb))
print('knn                 :')
print('  cross_val         : {} : std={} : {}'.format(score_knn.mean(), score_knn.std(), score_knn))
print('  test_data         : {}'.format(score_test_knn))
print('svm                 :')
print('  cross_val         : {} : std={} :  {}'.format(score_svm.mean(), score_svm.std(), score_svm))
print('  test_data         : {}'.format(score_test_svm))
print('svm_best            :')
print('  cross_val         : {} : std={} :  {}'.format(score_svm_best.mean(), score_svm_best.std(), score_svm_best))
print('  test_data         : {}'.format(score_test_svm_best))
print('ensamble            :')
print('  test_data         : {}'.format(score_test_ensamble))



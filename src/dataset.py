import pandas as pd
import numpy as np

class DataSetPart:
    def __init__(self):
        self.y = []
        self.x = []
        pass

class DataSet:
    def __init__(self, path, scaled=True, pca_components=None):
        features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'Width', 'Min', 'Max', 'Nmax', 'Nzeros', 'Mode',
                    'Mean', 'Median', 'Variance', 'Tendency', 'CLASS', 'NSP']
        df = pd.read_csv(path, sep=',', header=1, skiprows=0, skipinitialspace=True, engine='python')
        df = df[features]
        self.df = df
                
        self.train = DataSetPart()
        self.test = DataSetPart()

        from sklearn.model_selection import train_test_split
        for c in np.unique(df['CLASS'].values):
            rows = df[df['CLASS']==c].values[:, 0:-2]
            x_train, x_test, y_train, y_test = train_test_split(rows, np.full(len(rows), c), test_size=0.2, random_state=42)
            self.train.x.extend(x_train)
            self.train.y.extend(y_train)
            self.test.x.extend(x_test)
            self.test.y.extend(y_test)
        
        self.train.x = np.array(self.train.x)
        self.train.y = np.array(self.train.y)
        self.test.x = np.array(self.test.x)
        self.test.y = np.array(self.test.y)

        if pca_components:
            from sklearn.decomposition import PCA
            pca = PCA(n_components=pca_components)
            pca.fit(self.train.x)
            self.train.x = pca.transform(self.train.x)
            self.test.x = pca.transform(self.test.x)

        if scaled:
            from sklearn.preprocessing import StandardScaler
            scaler = StandardScaler()
            scaler.fit(self.train.x)
            self.train.x = scaler.transform(self.train.x)
            self.test.x = scaler.transform(self.test.x)
        

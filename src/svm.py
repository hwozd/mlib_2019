from dataset import DataSet

from sklearn import svm
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score
import numpy as np
from collections import defaultdict

MAX_ITER = 1000 # kernel='poly' takes too long to calculate, set some max number of iterations
import warnings
warnings.filterwarnings('ignore', 'Solver terminated early*')

# kernel from {‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’ }
# gamma from {'auto', 'scale'}
# decision_function_shape from {'ovo', 'ovr'}
def train_svm(dataset, kernel='rbf', gamma='auto', decision_function_shape='ovr', C=1, max_iter=MAX_ITER):
    clf = svm.SVC(gamma=gamma, kernel=kernel, C=C, decision_function_shape=decision_function_shape, max_iter=max_iter)
    score = cross_val_score(clf, dataset.train.x, dataset.train.y, cv=5, scoring='accuracy') 

    clf2 = svm.SVC(gamma=gamma, kernel=kernel, C=C, decision_function_shape=decision_function_shape, max_iter=max_iter)
    clf2.fit(dataset.train.x, dataset.train.y)
    test_y = clf2.predict(dataset.test.x)
    test_score = accuracy_score(dataset.test.y, test_y)

    return score, test_score

def search_svm_kernel(dataset, max_iter=MAX_ITER, decision_function_shape='ovr'):
    from sklearn import svm
    from sklearn.model_selection import cross_val_score
    from sklearn.metrics import accuracy_score

    d = {}
    for kernel in ['rbf', 'linear', 'poly', 'sigmoid']:
        clf = svm.SVC(gamma='auto', kernel=kernel, max_iter=max_iter)
        score = cross_val_score(clf, dataset.train.x, dataset.train.y, cv=5, scoring='accuracy')
        d[kernel] = '({}: {} +- {}: {}) '.format(kernel, score.mean(), score.std(), score)
    for _, v in d.items():
        print(v)


def grid_search_svm(dataset, kernel='linear', decision_function_shape='ovr'):
    from sklearn.model_selection import GridSearchCV
    from sklearn import svm
    import pandas as pd

    parameters = {'C':[0.3, 0.5, 1, 2, 10, 50, 100, 200 , 600, 1200, 3000, 6000, 10000, 20000, 30000, 40000, 80000, 100000, 200000, 400000], 
                  'gamma':[0.0001, 0.0012, 0.0025, 0.0037, 0.005, 0.01, 0.02, 0.05, 0.1, 0.2, 0.35, 0.5, 0.75, 1.0, 2.0, 3.0, 4.0]}

    svc = svm.SVC(kernel=kernel, max_iter=MAX_ITER, decision_function_shape=decision_function_shape)
    clf = GridSearchCV(svc, parameters, cv=5, n_jobs=4, return_train_score=True, iid=False)

    clf.fit(dataset.train.x, dataset.train.y)
    df = pd.DataFrame.from_dict(clf.cv_results_)
    df = df.sort_values(by='rank_test_score')
    print('best mean_test_score: ', df['mean_test_score'].values[0:5])
    print('best param_C: ', df['param_C'].values[0:5])
    print('best param_gamma', df['param_gamma'].values[0:5])
    return df

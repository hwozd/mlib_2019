from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score

def train_knn(dataset, k=3):
    clf = KNeighborsClassifier(n_neighbors=k)
    score = cross_val_score(clf, dataset.train.x, dataset.train.y, cv=5, scoring='accuracy') 

    clf2 = KNeighborsClassifier(n_neighbors=k)
    clf2.fit(dataset.train.x, dataset.train.y)
    test_y = clf2.predict(dataset.test.x)
    test_score = accuracy_score(dataset.test.y, test_y)

    return score, test_score
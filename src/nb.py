from sklearn import naive_bayes as nb
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score

def train_nb(dataset):
    clf = nb.GaussianNB()
    score = cross_val_score(clf, dataset.train.x, dataset.train.y, cv=5, scoring='accuracy')

    clf2 = nb.GaussianNB()
    clf2.fit(dataset.train.x, dataset.train.y)
    test_y = clf2.predict(dataset.test.x)
    test_score = accuracy_score(dataset.test.y, test_y)

    return score, test_score

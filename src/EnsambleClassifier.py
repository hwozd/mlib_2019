from sklearn.neighbors import KNeighborsClassifier
import numpy as np
from collections import defaultdict
from sklearn.metrics import accuracy_score
from sklearn import svm
from dataset import DataSet


class EnsambleClassifier:
    def fit(self, x, y, num_classifiers, create_classifier_fnc):
        sub_batch_size = int(len(y) * 0.7)
        clfs = []
        np.random.seed(42)
        for _ in range(num_classifiers):
            indices = np.random.randint(low=0, high=len(y), size=sub_batch_size)
            x_batch = x[indices, :]
            y_batch = y[indices]
            clf = create_classifier_fnc()
            clf.fit(x_batch, y_batch)
            clfs.append(clf)

        self.clfs = clfs
    
    def predict(self, datum, weights=None):
        votes = defaultdict(float)
        for i, clf in enumerate(self.clfs):
            c = clf.predict([datum])[0]
            weight = 1.0 if not weights else weights[i]
            votes[c] = votes[c] + weight
        maximum = max(votes, key=votes.get)
        return maximum
    
    def get_individual_accuracies(self, x, y):
        r = []
        for clf in self.clfs:
            y_pred = clf.predict(x)
            a = accuracy_score(y, y_pred)
            r.append(a)
        return r
    
    def accuracy(self, x, y, weights=None):
        y_pred = []
        for i in range(len(y)):
            datum = x[i, :] 
            y_pred.append(self.predict(datum, weights))
        return accuracy_score(y, y_pred)


def train_ensamble(dataset):
    def create_classifier():
        n = np.random.randint(low=0, high=10)
        if n < 2:
            k = np.random.randint(low=1, high=6)
            return KNeighborsClassifier(n_neighbors=k)
        else:
            parameters = {'C':[0.3, 0.5, 1, 2, 10, 50, 100, 200 , 600, 1200, 3000, 6000, 10000, 20000, 30000, 40000, 80000, 100000, 200000, 400000], 
                          'gamma':[0.0001, 0.0012, 0.0025, 0.0037, 0.005, 0.01, 0.02, 0.05, 0.1, 0.2, 0.35, 0.5, 0.75, 1.0, 2.0, 3.0, 4.0]}
            C = parameters['C'][np.random.randint(low=0, high=len(parameters['C']))]
            gamma = parameters['gamma'][np.random.randint(low=0, high=len(parameters['gamma']))]
            clf = svm.SVC(gamma=gamma, kernel='linear', C=C, decision_function_shape='ovo', max_iter=100000)
            return clf

    clf = EnsambleClassifier()
    num_classifiers = 50
    clf.fit(dataset.train.x, dataset.train.y, num_classifiers, create_classifier)
    weights = clf.get_individual_accuracies(dataset.train.x, dataset.train.y)
    weights = 1000 * weights
    weights = None # weights does not have any real impact
    return clf.accuracy(dataset.test.x, dataset.test.y, weights)
